package dto;

public class Electrodomestico {

	final String cons="F";
	final double prec=100;
	final String col="Blanco";
	final double pes=5;
	
	//==========================
	
	private double preciob;
	private String color;
	private String consumo;
	private double peso;
	
	//==========================
	
	// Un constructor por defecto.
	public Electrodomestico() {
		this.preciob=prec;
		this.color=col;
		this.consumo=cons;
		this.peso=pes;
		
	}
	
	// Un constructor con el precio y peso. El resto por defecto.
	public Electrodomestico(double preciob, double peso) {
		
		this.preciob=prec;
		this.color=col;
		this.consumo=cons;
		this.peso=pes;
	}
	
	// Un constructor con todos los atributos.
	public Electrodomestico(double preciob, String color, String consumo, double peso) {
		this.preciob = preciob;
		this.color = disponible(color);
		this.consumo = tconsumo(consumo);
		this.peso = peso;
	}
	
	
	//colores disponibles (blanco, negro, rojo, azul, gris) no importa mayusculas
	
	public static String disponible(String color) {
		
		if (color.equalsIgnoreCase("blanco")) {
			color = "Blanco";
		}
		else if(color.equalsIgnoreCase("negro")) {
			color = "Negro";
		}
		else if(color.equalsIgnoreCase("rojo")) {
			color = "Rojo";
		}
		else if(color.equalsIgnoreCase("azul")) {
			color = "Azul";
		}
		else if(color.equalsIgnoreCase("gris")) {
			color = "Gris";
		}
	
	return color;
	
	}
	
	//comporbar letra
	public static String tconsumo(String consumo) {
		
		if (consumo.equalsIgnoreCase("A")) {
			consumo="A";
		}
		else if (consumo.equalsIgnoreCase("B")) {
			consumo="B";
		}
		else if (consumo.equalsIgnoreCase("C")) {
			consumo="C";
		}
		else if (consumo.equalsIgnoreCase("D")) {
			consumo="D";
		}
		else if (consumo.equalsIgnoreCase("E")) {
			consumo="E";
		}
		else if (consumo.equalsIgnoreCase("F")) {
			consumo="F";
		}
		
		return consumo;
	}
	
	//getters
	
	public double getPreciob() {
		return preciob;
	}

	public String getColor() {
		return color;
	}

	public String getConsumo() {
		return consumo;
	}

	public double getPeso() {
		return peso;
	}

	//setters
	
	public void setPreciob(double preciob) {
		this.preciob = preciob;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setConsumo(String consumo) {
		this.consumo = consumo;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	@Override
	public String toString() {
		return "Electrodomestico [preciob=" + preciob + ", color=" + color + ", consumo=" + consumo + ", peso=" + peso
				+ "]";
	}
	
	
	
}
