# POO . 3.Electrodomestico

Ejercicio 3

3. Crearemos una clase llamada Electrodomestico con las siguientes características:
• Sus atributos son precio base, color, consumo energético (letras entre A y F) ypeso.
Indica que se podrán heredar.
• Por defecto, el color sera blanco, el consumo energético sera F, el precioBase es de
100 € y el peso de 5 kg. Usa constantes para ello.
• Los colores disponibles son blanco, negro, rojo, azul y gris. No importa si el nombre
esta en mayúsculas o en minúsculas.
• Los constructores que se implementaran serán
o Un constructor por defecto.
o Un constructor con el precio y peso. El resto por defecto.
o Un constructor con todos los atributos.